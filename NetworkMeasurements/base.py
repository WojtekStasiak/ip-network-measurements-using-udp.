"""This file contains Base class for Client and Server implemntation
    with methods used in the classes
"""

import socket
import logging
import sys
import struct
import time
import logging.handlers
from typing import Tuple
from abc import ABC
from ipaddress import ip_address
from ipaddress import IPv4Address
from ipaddress import IPv6Address
from ipaddress import AddressValueError
from collections import OrderedDict


def set_logging() -> Tuple[logging.getLogger, logging.Formatter]:
    """Helps to initialize logging in classes, adds logging to stdout"""

    log_formatter = logging.Formatter(
        "%(asctime)s [%(threadName)-12.12s] \
        [%(levelname)-5.5s]  %(message)s"
    )
    root_logger = logging.getLogger()
    root_logger.setLevel(level=logging.INFO)

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)

    return (root_logger, log_formatter)


def current_timestamp() -> float:
    """Time based on CPU, only for this pc"""
    return float("{:.4f}".format(time.perf_counter()))


def time_compare(time1: float, time2: float) -> int:
    """Returns 1 if time1 is earlier than time2, else returns -1, returns 0 if equal"""

    if time1 < time2:
        return 1

    elif time1 > time2:
        return -1

    else:
        return 0


def time_diff(time1: float, time2: float) -> float:
    """Returns difference between time1 and time2"""

    if time_compare(time1, time2) == 0:
        return 0

    elif time_compare(time1, time2) == 1:
        return time2 - time1

    elif time_compare(time1, time2) == -1:
        return time1 - time2



class Base(ABC):
    """ Base for Client and Server """

    def __init__(self, args_dict: dict):
        self.port = args_dict["p"]
        self.bandwidth = args_dict["b"]
        self.buffer_size = args_dict["w"]
        self.block_size = args_dict["l"]
        self.reporting_interval = args_dict["i"]
        self.address_to_bind = args_dict["B"]
        self.v6 = args_dict["v6"]
        self.bytes_to_trasmit = args_dict["n"]
        self.duration = args_dict["t"]
        self.multicast = args_dict["m"]

        self.address_to_connect = args_dict["c"]
        self.daemon = args_dict["d"]
        #self.rname = args_dict["r"]

        self.sock = socket.SOCK_DGRAM  # set UDP

        # logging and address family
        self.__set_logging()
        self.__set_af()


    def __set_af(self):
        """Sets address family for v4 or v6"""

        if self.v6:
            self.af = socket.AF_INET6
            self.root_logger.info("Address family IPv6 set")

        else:
            self.af = socket.AF_INET
            self.root_logger.info("Address family IPv4 set")


    def __set_logging(self):
        """Sets logging to file and stdout"""

        self.root_logger, self.log_formatter = set_logging()
        self.file_handler = logging.FileHandler(self.__class__.__name__ + ".log")
        self.file_handler.setFormatter(self.log_formatter)
        self.root_logger.addHandler(self.file_handler)







