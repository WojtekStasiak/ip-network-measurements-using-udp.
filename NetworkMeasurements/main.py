import socket
import sys
import argparse
import daemon
from ipaddress import ip_address, IPv4Address, IPv6Address
from Server import Server
from Client import Client


def parse_arguments():
    parser = argparse.ArgumentParser(description='UDP Client/Server')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        '-c', help='Run UDP client, Usage: -c N.N.N.N', type=str)
    group.add_argument(
        '-s', help='Run UDP server, Usage: -s', action='store_true')
    parser.add_argument('-p', help='Specify port', default=5000, type=int)
    parser.add_argument(
        '-b', help='Specify bandwidth [KB]', type=int, default=2 * (10 ** 3))
    parser.add_argument(
        '-w', help='Specify buffer size [B]', type=int, default=10000)
    parser.add_argument(
        '-l', help='Specify block size [B]', type=int, default=2048)
    parser.add_argument(
        '-i', help='Reporting interval [s]', type=float, default=5)
    parser.add_argument('-B', help='Specify address to bind', type=ip_address)
    parser.add_argument(
        '-v6', help='Use IPv6 for measurements', action='store_true')
    parser.add_argument('-n', help='Specify much bytes to send', type=int)
    parser.add_argument(
        '-t', help='Specify length of test [s]', type=int, default=10)
    parser.add_argument('-m', help='Multicast group address', type=ip_address)
    parser.add_argument('-d', help='Run server as a daemon',
                        action='store_true')
    
    args = parser.parse_args()
    args_dict = vars(args)
    print(args_dict)

    if args.m:
        if not args_dict['m'].is_multicast:
            parser.error("Wrong multicast address")

    if args.v6:
        if args.B and not IPv6Address(args_dict['B']):
            parser.error("Wrong IPv6 address format")

    else:
        if args.B and not IPv4Address(args_dict['B']):
            parser.error("Wrong IPv4 address format")

    return args


if __name__ == "__main__":
    args = parse_arguments()
    args_dict = vars(args)

    if args.s:
        server = Server(args_dict)
        if args.d:  # run as a daemon
            with daemon.DaemonContext():
                server.start()

        else:
            server.start()

    elif args.c:
        client = Client(args_dict)
        client.send()