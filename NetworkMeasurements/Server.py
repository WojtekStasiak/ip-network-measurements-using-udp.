"""Implementation of UDP server class"""

from base import *


class Server(Base):
    """Server is waiting for data from Client and calculates bandwidth, 
       jitter and packet loss
    """

    def __init__(self, args_dict: dict):
        super().__init__(args_dict)
        self.total_bytes_received = 0
        self.bytes_received = 0  # bytes received in 'reporting_interval'
        self.store_calculated = OrderedDict() # storing bytes, jitter and packet loss every reporting_inverval
        self.packet_times = {"pp_time": 0, "pp_ar_time": 0, "cp_time": 0, "cp_ar_time": 0}
        self.packets_lost = 0
        self.highest_sequence = 0
        self.received_sequence = 0
        self.jitter = 0
        self.jitters = []
        self.last_sequence = 0
        self.rec_buffer_size = 0
        self.rec_block_size = 0
        
        if not self.address_to_connect:
            if self.address_to_bind:  # if 'B' specified
                self.address_to_connect = str(self.address_to_bind), self.port

            else:
                if self.v6:
                    self.address_to_connect = ("::", self.port)

                else:
                    self.address_to_connect = ("0.0.0.0", self.port)

        if self.daemon:
            self.sys_log_handler = logging.handlers.SysLogHandler(address='/dev/log')
            self.root_logger.addHandler(self.sys_log_handler)
            self.root_logger.info("Server as a daemon, logging to syslog")


    def __store_data(self, track_interval: int, processing_time: float):
        """Writes data to 'self.store_calculate' every 'self.reporting_interval'"""

        self.jitter = sum(self.jitters) / len(self.jitters)
        self.store_calculated[track_interval] = (
            self.bytes_received,
            self.bytes_received / processing_time,
            self.packets_lost,
            self.jitter,
        )
        self.bytes_received = 0


    def __print_data(self, track_interval: int):
        """Prints data if not first packet"""

        if track_interval:
            self.root_logger.info(
                "{} sec   {} Bytes   {:.4f} KBytes/sec    {} packets lost     {:.6f} jitter"
                .format(
                    str(track_interval * self.reporting_interval),  # interval
                    self.store_calculated[track_interval][0],       # bytes received
                    self.store_calculated[track_interval][1] / (10**3), # bandwidth 
                    self.store_calculated[track_interval][2],       # packets lost
                    self.store_calculated[track_interval][3],       # jitter
                )
            )


    def __calculate_packet_loss(self):
        """Calculates how many packets got lost based on sequence number"""

        supposed_sequence = self.highest_sequence + 1
        if self.received_sequence == supposed_sequence:
            pass

        elif self.received_sequence > supposed_sequence:
            self.packets_lost += 1


    def __calculate_jitters(self):
        """Calculates jitter between two packets"""

        pp_time = self.packet_times["pp_time"]
        pp_ar_time = self.packet_times["pp_ar_time"]
        cp_time = self.packet_times["cp_time"]
        cp_ar_time = self.packet_times["cp_ar_time"]

        if pp_time:
            m1 = abs(pp_ar_time - pp_time)
            m2 = abs(cp_ar_time - cp_time)
            jitter = m2 - m1
            self.jitters.append(jitter)

        else:
            jitter = 0
            self.jitters.append(jitter)
    

    def __process_packet(self, data: bytearray):
        """Gets timestamp, sequence, buffer size and block size
           from packet and writes it to self.packet_times, self.last_sequence
           self.rec_buffer_size, self.rec_block_size
           """

        cp_time = data[:4]          # first 4B - timestamp
        rec_sequence = data[4:8]    # second 4B - sequence number
        buffer_size = data[8:12]        # padding to fulfill blocksize
        block_size = data[12:16]

        cp_time = struct.unpack(">f", cp_time)[0]
        rec_sequence = struct.unpack(">i", rec_sequence)[0]
        buffer_size = struct.unpack(">i", buffer_size)[0]
        block_size = struct.unpack(">i", block_size)[0]
        self.last_sequence = rec_sequence
        
        self.packet_times['pp_time'] = self.packet_times['cp_time']
        self.packet_times['pp_ar_time'] = self.packet_times['cp_ar_time']

        self.packet_times['cp_time'] = cp_time
        self.packet_times['cp_ar_time'] = current_timestamp()
        
        self.rec_block_size = block_size
        self.rec_buffer_size = buffer_size


    def __join_multicast(self, server_socket: socket.socket):
        """Join multicast group on all the interfaces"""

        if self.multicast:
            mreq = struct.pack(
                "4sl", socket.inet_aton(str(self.multicast)), socket.INADDR_ANY)
            server_socket.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)


    def __receive_data(self, server_socket: socket.socket, start: float):
        track_interval = 0
        while True:

            data, addr = server_socket.recvfrom(self.block_size)
            if not self.total_bytes_received:
                """if first packet, log from whom"""
                self.root_logger.info(
                    "Received connection from: {}, port: {}".format(addr[0], addr[1]))

            self.root_logger.debug("Received: {} bytes".format(data))
            self.total_bytes_received += len(data)
            self.bytes_received += len(data)
            self.__process_packet(data)
            self.__calculate_jitters()
            self.__calculate_packet_loss()
            end = time.time()

            processing_time = end - start

            if processing_time >= self.reporting_interval:
                self.__store_data(track_interval, processing_time)
                self.__print_data(track_interval)
                start = time.time()
                track_interval += 1


    def __receive_test(self, server_socket: socket.socket, start: float):
        """To receive test sequence"""
        display_info = True

        while self.last_sequence < 10:
            data, addr = server_socket.recvfrom(self.block_size)

            self.__process_packet(data)
            if self.rec_buffer_size:
                server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, self.rec_buffer_size)  
                server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, self.rec_buffer_size)  

            if display_info:
                self.root_logger.info(
                    f"Socket created {str(self.af)}, {str(self.sock)}")

                self.root_logger.info(
                    f"Socket bound to address: {self.address_to_connect[0]}, port: {self.address_to_connect[1]}")

                self.root_logger.info(
                    f"Receiving buffer: {server_socket.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF)} Bytes")

                self.root_logger.info(
                    f"Receiving buffer: {server_socket.getsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF)} Bytes")

                self.root_logger.info(f"Reading from socket: {self.block_size} Bytes")

                display_info = False


    def start(self):
        """Creates socket, binds to specific address, then waiting for data"""

        self.root_logger.info("Starting server")

        server_socket = socket.socket(self.af, self.sock)
        server_socket.bind(self.address_to_connect)
        self.__join_multicast(server_socket)

        start = time.time()
        self.__receive_test(server_socket, start)
        self.__receive_data(server_socket, start)