"""Implementation of UDP server class"""

from base import *



class Client(Base):
    """Responsible for datagram creation and sending to the server"""

    def __init__(self, args_dict: dict):
        super().__init__(args_dict)

        self.time_to_send: float  # how often send datagram
        self.__calculate_parameters()
        
        self.__set_address()
        

    def __calculate_parameters(self):
        """To align self.bandwidth, self.duration and self.bytes_to_trasmit"""

        if self.bandwidth and self.bytes_to_trasmit:
            self.root_logger.warning('-b and -n specified, ignoring "-t"')
            self.duration = self.bytes_to_trasmit / self.bandwidth

        elif self.bandwidth and self.duration:
            logging.warning('-b and -t specified, ignoring "-n"')
            self.bytes_to_trasmit = self.bandwidth * self.duration

        elif not self.bandwidth:
            logging.warning('-n and -t specified, calculating "-b"')
            self.bandwidth = self.bytes_to_trasmit / self.duration

        if self.bandwidth:
            # to fulfill 'bandwidth', send every 'time_to_send' 'buf_size' bytes
            self.time_to_send = self.block_size / self.bandwidth

        self.root_logger.info(
            "Duration: {}, bytes to trasmit: {}, bandwidth: {}".format(
                self.duration, self.bytes_to_trasmit, self.bandwidth
            )
        )


    def __set_address(self):
        try:
            if self.v6:
                self.address_to_connect = str(IPv6Address(self.address_to_connect)), self.port
            
            else:
                self.address_to_connect = str(IPv4Address(self.address_to_connect)), self.port
        
        except AddressValueError:
            try:
                self.address_to_connect = socket.gethostbyname(self.address_to_connect), self.port
            
            except:
                self.root_logger.fatal("Cannot traslate name to IP")

            
    def __pack_to_c_types(self, sequence: int) -> Tuple[bytes, bytes]:
        """Converting Python types to C types"""

        sequence_bytes = struct.pack(">i", sequence)

        timestamp = current_timestamp()

        # pack timestamp to 4B float
        timestamp_bytes = struct.pack(">f", timestamp)

        return sequence_bytes, timestamp_bytes


    def __create_datagram(
        self, sequence: int, payload: bytearray) -> Tuple[bytearray, int, bytearray]:

        sequence_bytes, timestamp_bytes = self.__pack_to_c_types(sequence)
        sequence_size = len(sequence_bytes)
        timestamp_size = len(timestamp_bytes)

        # padding - reduced bytes_to_trasmit by timestamp in each send fragment
        padding = (
            bytearray(timestamp_size + sequence_size)
            + payload[: self.block_size - (timestamp_size + sequence_size)]
        )
        payload = payload[self.block_size - (timestamp_size + sequence_size) :]

        self.root_logger.info(
            "Padding without timestamp and sequence number: {}, payload left: {}".format(
                len(padding), len(payload)
            )
        )

        datagram = sequence_bytes + timestamp_bytes + padding
        sequence += 1

        return datagram, sequence, payload


    def __create_payload(self) -> bytearray:
        """Padding callculated for all the datagrams"""

        sequence_bytes, timestamp_bytes = self.__pack_to_c_types(sequence=0)

        sequence_size = len(sequence_bytes)
        timestamp_size = len(timestamp_bytes)

        payload = bytearray(
            self.bytes_to_trasmit
            - (
                int(self.bytes_to_trasmit / self.block_size)
                * (timestamp_size + sequence_size)
            )
        )
        return payload


    def __send_data(self, client_socket: socket.socket, payload: bytes, sequence: int):
        while payload:
            start = time.time()

            datagram, sequence, payload = self.__create_datagram(sequence, payload)
            client_socket.sendto(datagram, self.address_to_connect)

            end = time.time()
            delay = end - start  # should be consider

            self.root_logger.info(
                "Createing and sending took: {}, send every: {}".format(
                    delay, self.time_to_send
                )
            )

            to_wait = time.time() + self.time_to_send

            while time.time() < to_wait:
                continue


    def __send_test(self, client_socket: socket.socket, payload: bytes, sequence: int):
        """Sending testing sequence in order for server to set up buffer sizes"""

        while sequence < 10:
            datagram, sequence, payload = self.__create_datagram(sequence, payload)
            client_socket.sendto(datagram, self.address_to_connect)
            self.root_logger.info("Sent datagram with sequence {}".format(sequence))
        return sequence


    def send(self):
        """Creates socket, creates datagram, and sends datagram"""

        self.root_logger.info(
            "Starting testing connection, duration: {}".format(self.duration)
        )

        client_socket = socket.socket(self.af, self.sock)
        client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 
            int(self.buffer_size/2))        #dont know why "/2" but it works

        client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 
            int(self.buffer_size/2))

        if self.address_to_bind:
            client_socket.bind((str(self.address_to_bind), self.port))

        self.root_logger.info(
            f"Socket created {str(self.af)}, {str(self.sock)}"
        )
        self.root_logger.info(
            f"Receiving buffer: {client_socket.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF)}"
        )
        self.root_logger.info(
            f"Sending buffer: {client_socket.getsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF)}"
        )

        payload = self.__create_payload()
        sequence = self.__send_test(client_socket, payload, sequence=0)
        time.sleep(3)
        
        self.__send_data(client_socket, payload, sequence)
        self.root_logger.info("Data trassmitted")