# [Network programming] - Testing quality of connection in IP networks using UDP protocol - "qualiPy.py"

## Overview
### "qualiPy" is a simple client - server command line tool for performing network quality tests such as bandwidth measurement, jitter, packet loss using UDP protocol

## How to use
### `qualiPy.py [-s|-s daemon|-c host] [options]`

#### OPTIONS: 
```
-p set specific port
-b set bandwitdh [kB/s]
-w set socket buffer size [B]
-l set the qualiPy buffer [B]
-i reporting interval [s]
-B bind to specific interfaces 
-m join multicast group
-v6 IPv6 test

client specific:
-b set bandwitdh [kB/s] 
-n number of bytes to transmit 
-t length of test [s]
-m multicast group
```

